use std::{thread, time::Duration};

use discord_rich_presence::{activity::Activity, new_client, DiscordIpc};
use mpris::{Metadata, PlayerFinder};

fn main() {
    let mut client = new_client("901142090430447656").expect("Failed to create RPC client!");

    let player_finder = PlayerFinder::new().expect("Could not connect to DBus");

    let mut connected = false;

    // Daemon loop
    loop {
        if let Ok(players) = player_finder.find_all() {
            for player in players {
                if player.can_play().unwrap_or(false) {
                    if let Ok(metadata) = player.get_metadata() {
                        if !connected {
                            connected = client.connect().is_ok();
                            println!("Connected to discord: {}", connected);
                        } else {
                            connected = update_playing(metadata, &mut client);
                        }
                    }
                }
            }
        }
        thread::sleep(Duration::new(5, 0));
    }
}

fn update_playing<T: DiscordIpc>(metadata: Metadata, client: &mut T) -> bool {
    let album_artist;
    if let Some(artists) = metadata.album_artists() {
        album_artist = artists.join(", ");
    } else {
        album_artist = "Unknown Artist".to_string();
    }

    let title = metadata.title().unwrap_or("Unknown Track");
    let number = metadata.track_number().unwrap_or(1);
    let album = metadata.album_name().unwrap_or("Unknown Album");

    client
        .set_activity(
            Activity::new()
                .details(format!("Listening to {} by {}", title, album_artist).as_str())
                .state(format!("Track {} from {}", number, album).as_str()),
        )
        .is_ok()
}
